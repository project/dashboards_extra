<?php

namespace Drupal\dashboards_extra\Plugin\Dashboard;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\Query\QueryAggregateInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dashboards\Plugin\Dashboard\ChartTrait;
use Drupal\dashboards\Plugin\DashboardBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Show account info.
 *
 * @Dashboard(
 *   id = "my_content_statistics",
 *   label = @Translation("My Content Statistics"),
 *   category = @Translation("Dashboards: Extras"),
 * )
 */
class MyContentStatistics extends DashboardBase {
  use ChartTrait;

  /**
   * Entity query.
   *
   * @var \Drupal\Core\Entity\Query\QueryAggregateInterface
   */
  protected $entityQuery;

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeInfo;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CacheBackendInterface $cache, QueryAggregateInterface $query_factory, EntityTypeBundleInfoInterface $entity_type_info) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $cache);
    $this->entityQuery = $query_factory;
    $this->entityTypeInfo = $entity_type_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('dashboards.cache'),
      $container->get('entity_type.manager')->getStorage('node')->getAggregateQuery('AND'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildSettingsForm(array $form, FormStateInterface $form_state, array $configuration): array {
    $all_bundle_info = $this->entityTypeInfo->getAllBundleInfo();
    $all_content_types = $all_bundle_info["node"];
    foreach ($all_content_types as $machine_name => $content_type) {
      $content_type_label[$machine_name] = $content_type["label"];
    }
    $form['chart_type'] = [
      '#type' => 'select',
      '#options' => $this->getAllowedStyles(),
      '#default_value' => (isset($configuration['chart_type'])) ? $configuration['chart_type'] : 'pie',
    ];
    $form['node_type'] = [
      '#title' => $this->t('Choose Content Type'),
      '#type' => 'select',
      '#multiple' => TRUE,
      '#required' => TRUE,
      '#options' => $content_type_label,
      '#default_value' => $configuration['node_type'],
    ];
    $form['publish'] = [
      '#title' => $this->t('Published Content'),
      '#type' => 'checkbox',
      '#default_value' => (isset($configuration['publish'])) ? $configuration['publish'] : TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRenderArray($configuration): array {
    if (isset($configuration['chart_type'])) {
      $this->setChartType($configuration['chart_type']);
    }
    if (isset($configuration['node_type'])) {
      foreach ($configuration['node_type'] as $type) {
        $node_type[] = $type;
      }
    }
    $query = $this->entityQuery
      ->condition('type', $node_type, "IN")
      ->condition('status', $configuration['publish'])
      ->condition('langcode', \Drupal::languageManager()->getCurrentLanguage()->getId())
      ->condition('uid', \Drupal::currentUser()->id())
      ->groupBy('type')
      ->aggregate('nid', 'COUNT')
      ->accessCheck(FALSE);

    $results = $query->execute();
    $types = $this->entityTypeInfo->getBundleInfo('node');
    $rows = [];

    foreach ($results as $result) {
      $rows[] = [
        $types[$result['type']]['label'],
        $result['nid_count'],
      ];
    }

    if (empty($results)) {
      $rows = [];
      $this->setEmpty(TRUE);
    }

    $this->setLabels([
      $this->t('Node Type'),
      $this->t('Count'),
    ]);

    $this->setRows($rows);

    return $this->renderChart($configuration);
  }

}
