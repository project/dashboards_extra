<?php

namespace Drupal\dashboard_extra\Plugin\Dashboard;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\Query\QueryAggregateInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dashboards\Plugin\Dashboard\ChartTrait;
use Drupal\dashboards\Plugin\DashboardBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Show account info.
 *
 * @Dashboard(
 *   id = "users_statistics",
 *   label = @Translation("Users Statistics"),
 *   category = @Translation("Dashboards: Extras"),
 * )
 */
class UsersStatistics extends DashboardBase {
  use ChartTrait;

  /**
   * Entity query.
   *
   * @var \Drupal\Core\Entity\Query\QueryAggregateInterface
   */
  protected $entityQuery;

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeInfo;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CacheBackendInterface $cache, QueryAggregateInterface $query_factory, EntityTypeBundleInfoInterface $entity_type_info) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $cache);
    $this->entityQuery = $query_factory;
    $this->entityTypeInfo = $entity_type_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('dashboards.cache'),
      $container->get('entity_type.manager')->getStorage('user')->getAggregateQuery('AND'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildSettingsForm(array $form, FormStateInterface $form_state, array $configuration): array {
    $form['chart_type'] = [
      '#type' => 'select',
      '#options' => $this->getAllowedStyles(),
      '#default_value' => (isset($configuration['chart_type'])) ? $configuration['chart_type'] : 'pie',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRenderArray($configuration): array {
    if (isset($configuration['chart_type'])) {
      $this->setChartType($configuration['chart_type']);
    }

    $results = $this->entityQuery
      ->groupBy('roles')
      ->aggregate('uid', 'COUNT')
      ->accessCheck(FALSE)
      ->execute();

    $types = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();
    $rows = [];

    foreach ($results as $result) {
      if (isset($r["roles_target_id"])) {
        $rows[] = [
          $types[$result["roles_target_id"]]->label(),
          $result['uid_count'],
        ];
      }
    }

    if (empty($result)) {
      $rows = [];
      $this->setEmpty(TRUE);
    }

    $this->setLabels([
      $this->t('Roles'),
      $this->t('Count'),
    ]);

    $this->setRows($rows);

    return $this->renderChart($configuration);
  }

}
