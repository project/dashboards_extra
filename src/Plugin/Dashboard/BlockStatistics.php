<?php

namespace Drupal\dashboards_extra\Plugin\Dashboard;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\Query\QueryAggregateInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dashboards\Plugin\Dashboard\ChartTrait;
use Drupal\dashboards\Plugin\DashboardBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Show account info.
 *
 * @Dashboard(
 *   id = "block_statistics",
 *   label = @Translation("Block Statistics"),
 *   category = @Translation("Dashboards: Extras"),
 * )
 */
class BlockStatistics extends DashboardBase {
  use ChartTrait;

  /**
   * Entity query.
   *
   * @var \Drupal\Core\Entity\Query\QueryAggregateInterface
   */
  protected $entityQuery;

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeInfo;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CacheBackendInterface $cache, QueryAggregateInterface $query_factory, EntityTypeBundleInfoInterface $entity_type_info) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $cache);
    $this->entityQuery = $query_factory;
    $this->entityTypeInfo = $entity_type_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('dashboards.cache'),
      $container->get('entity_type.manager')->getStorage('block_content')->getAggregateQuery('AND'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildSettingsForm(array $form, FormStateInterface $form_state, array $configuration): array {
    $all_bundle_info = $this->entityTypeInfo->getAllBundleInfo();
    $all_block_types = $all_bundle_info["block_content"];
    foreach ($all_block_types as $machine_name => $block_type) {
      $block_type_label[$machine_name] = $block_type["label"];
    }
    $form['chart_type'] = [
      '#type' => 'select',
      '#options' => $this->getAllowedStyles(),
      '#default_value' => (isset($configuration['chart_type'])) ? $configuration['chart_type'] : 'pie',
    ];
    $form['block_type'] = [
      '#title' => $this->t('Choose block Type'),
      '#type' => 'select',
      '#multiple' => TRUE,
      '#required' => TRUE,
      '#options' => $block_type_label,
      '#default_value' => $configuration['block_type'],
    ];
    $form['publish'] = [
      '#title' => $this->t('Published Block Content'),
      '#type' => 'checkbox',
      '#default_value' => (isset($configuration['publish'])) ? $configuration['publish'] : TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRenderArray($configuration): array {
    if (isset($configuration['chart_type'])) {
      $this->setChartType($configuration['chart_type']);
    }
    if (isset($configuration['block_type'])) {
      foreach ($configuration['block_type'] as $type) {
        $block_type[] = $type;

      }
    }
    $query = $this->entityQuery
      ->condition('type', $block_type, "IN")
      ->condition('status', $configuration['publish'])
      ->groupBy('type')
      ->groupBy('langcode')
      ->aggregate('id', 'COUNT')
      ->accessCheck(FALSE);

    $results = $query->execute();
    $types = $this->entityTypeInfo->getBundleInfo('block_content');
    $rows = [];

    foreach ($results as $result) {
      if ($result["langcode"] === \Drupal::languageManager()->getCurrentLanguage()->getId()) {
        $rows[] = [
          $types[$result['type']]['label'],
          $result['id_count'],
        ];
      }
    }

    if (empty($results)) {
      $rows = [];
      $this->setEmpty(TRUE);
    }

    $this->setLabels([
      $this->t('Block Type'),
      $this->t('Count'),
    ]);

    $this->setRows($rows);

    return $this->renderChart($configuration);
  }

}

