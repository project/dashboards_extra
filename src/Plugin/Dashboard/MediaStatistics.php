<?php

namespace Drupal\dashboard_extra\Plugin\Dashboard;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\Query\QueryAggregateInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dashboards\Plugin\Dashboard\ChartTrait;
use Drupal\dashboards\Plugin\DashboardBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Show account info.
 *
 * @Dashboard(
 *   id = "media_statistics",
 *   label = @Translation("Media Statistics"),
 *   category = @Translation("Dashboards: Extras"),
 * )
 */
class MediaStatistics extends DashboardBase {
  use ChartTrait;

  /**
   * Entity query.
   *
   * @var \Drupal\Core\Entity\Query\QueryAggregateInterface
   */
  protected $entityQuery;

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeInfo;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CacheBackendInterface $cache, QueryAggregateInterface $query_factory, EntityTypeBundleInfoInterface $entity_type_info) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $cache);
    $this->entityQuery = $query_factory;
    $this->entityTypeInfo = $entity_type_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('dashboards.cache'),
      $container->get('entity_type.manager')->getStorage('media')->getAggregateQuery('AND'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildSettingsForm(array $form, FormStateInterface $form_state, array $configuration): array {
    $all_bundle_info = $this->entityTypeInfo->getAllBundleInfo();
    $all_media_types = $all_bundle_info["media"];
    foreach ($all_media_types as $machine_name => $media_type) {
      $media_type_label[$machine_name] = $media_type["label"];
    }
    $form['chart_type'] = [
      '#type' => 'select',
      '#options' => $this->getAllowedStyles(),
      '#default_value' => (isset($configuration['chart_type'])) ? $configuration['chart_type'] : 'pie',
    ];
    $form['media_type'] = [
      '#title' => $this->t('Choose Media Type'),
      '#type' => 'select',
      '#multiple' => TRUE,
      '#required' => TRUE,
      '#options' => $media_type_label,
      '#default_value' => $configuration['media_type'],
    ];
    $form['publish'] = [
      '#title' => $this->t('Published Media'),
      '#type' => 'checkbox',
      '#default_value' => (isset($configuration['publish'])) ? $configuration['publish'] : TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRenderArray($configuration): array {
    if (isset($configuration['chart_type'])) {
      $this->setChartType($configuration['chart_type']);
    }
    if (isset($configuration['media_type'])) {
      foreach ($configuration['media_type'] as $type) {
        $media_type[] = $type;
      }
    }
    $results = $this->entityQuery
      ->condition('bundle', $media_type, "IN")
      ->condition('status', $configuration['publish'])
      ->groupBy('bundle')
      ->aggregate('mid', 'COUNT')
      ->accessCheck(FALSE)
      ->execute();

    $types = $this->entityTypeInfo->getBundleInfo('media');
    $rows = [];

    foreach ($results as $result) {
      $rows[] = [
        $types[$result['bundle']]['label'],
        $result['mid_count'],
      ];
    }

    if (empty($result)) {
      $rows = [];
      $this->setEmpty(TRUE);
    }

    $this->setLabels([
      $this->t('Media Type'),
      $this->t('Count'),
    ]);

    $this->setRows($rows);

    return $this->renderChart($configuration);
  }

}
