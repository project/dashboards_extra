## Module Description

This module extends the **Dashboard Extra** module, which itself depends on the **Dashboard** module, by adding additional block statistics for enhanced administrative insights.

### Key Features

- **Content Statistics**:
  Provides an overview of the content types, published/unpublished status, and recent content changes.
- **Block Statistics**:
  Offers detailed information about the block count across the site.
- **Media Statistics**:
  Displays detailed information about the media count across the site.
- **User Statistics**:
  Presents detailed information about the user count across the site.

### Purpose

This module delivers a more comprehensive and customizable dashboard for administrators, improving site management efficiency and providing valuable insights into key site metrics.
